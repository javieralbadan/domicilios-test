
const pkg = require('./package')

export default {
  mode: 'spa',
  /*
  ** Headers of the page
  */
  head: {
    title: process.env.npm_package_name || '',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: process.env.npm_package_description || '' },
      { name: 'keywords', content: 'Merqueo, frontend, prueba tecnica' },
      { name: 'author', content: 'Javier Albadán' },
      { name: 'theme-color', content: '#267871' },
      { name: 'msapplication-navbutton-color', content: '#267871' },
      { name: 'apple-mobile-web-app-status-bar-style', content: '#267871' },
      // Open Graph data
      { property: 'og:site_name', content: 'Domicilios Test' },
      { property: 'og:title', content: `${pkg.name} · ${pkg.description}` },
      { property: 'og:type', content: 'website' },
      { property: 'og:url', content: 'http://www.domicilios.albadan.co/' },
      { property: 'og:image', content: '/favicon.png' },
      { property: 'og:description', content: `${pkg.description}` },
      { property: 'og:locale', content: 'es_CO' },
      // Schema.org markup for Google+
      { itemprop: 'name', content: 'Domicilios Test' },
      { itemprop: 'description', content: `${pkg.description}` },
      { itemprop: 'image', content: '/favicon.png' },
      { hid: 'description', name: 'description', content: `${pkg.description}` }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.png' },
      { rel: 'stylesheet', href: 'https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.3.2/semantic.min.css' }
    ],
    script: [
      { src: 'https://cdnjs.cloudflare.com/ajax/libs/lodash.js/4.17.15/lodash.min.js' }
    ]
  },
  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#267871' },
  /*
  ** Global CSS
  */
  css: [
    '@/assets/css/main.css',
    '@/assets/css/vivify.min.css'
  ],
  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
    { src: '~/plugins/main', ssr: false },
    { src: '~/plugins/firebase', ssr: false },
    { src: '~/plugins/vue-moment', ssr: false },
    { src: '~/plugins/v-lazy-image', ssr: false },
    { src: '~/plugins/router-after-each', ssr: false },
    { src: '~/plugins/intersection-observer', ssr: false },
    { src: '~/plugins/vue-observe-visibility', ssr: false },
  ],
  /*
  ** Nuxt.js dev-modules
  */
  buildModules: [
    // Doc: https://github.com/nuxt-community/eslint-module
    '@nuxtjs/eslint-module'
  ],
  /*
  ** Nuxt.js modules
  */
  modules: [
    ['vue-scrollto/nuxt', { offset: -70 }],
  ],
  /*
  ** Error page generation folder
  */
  generate: {
    routes: ['404']
  },
  /*
  ** Build configuration
  */
  build: {
    /*
    ** You can extend webpack config here
    */
    extend (config, ctx) {
    }
  }
}
