import * as firebase from 'firebase/app'
import 'firebase/database'

const config = {
  apiKey: 'AIzaSyBMRbZYYSUN9FB695XR6lGRtPvZGpEIDB4',
  authDomain: 'domicilios-test.firebaseapp.com',
  databaseURL: 'https://domicilios-test.firebaseio.com',
  projectId: 'domicilios-test',
  storageBucket: '',
  messagingSenderId: '124359165675',
  appId: '1:124359165675:web:7839f6f3343254b7b820ae'
}

if (!firebase.apps.length) {
  firebase.initializeApp(config)
}

const database = firebase.database()

export default ({}, inject) => {
  inject('db', database)
}
