import Vue from 'vue'

// Methods
Vue.mixin({
	methods: {
		getColor (_reaction) {
			switch (_reaction) {
				case 'like':
					return 'blue'
					break
				
				case 'love':
					return 'red'
					break
				
				case 'dislike':
					return 'yellow'
					break
			}
		},
		getIcon (_reaction) {
			switch (_reaction) {
				case 'like':
					return 'thumbs up popInBottom'
					break
				
				case 'love':
					return 'heart spinIn delay-250'
					break
				
				case 'dislike':
					return 'thumbs down popInTop delay-500'
					break
			}
		}
	}
})