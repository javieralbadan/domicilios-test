export default ({ app }) => {
  // console.log('cambio de vista - app', app)
  app.router.beforeEach((to, from, next) => {
    // console.log('beforeEach', to, from)
    // Revisión y cargue de datos en store
    // console.log('store', app.store.state)
    if (!app.store.state.users) {
      app.store.dispatch('loadUsers')
    }
    if (!app.store.state.posts) {
      app.store.dispatch('loadPosts')
    }
    next()
  })
  app.router.afterEach((to, from) => {
    // console.log('to', to)
    // console.log('from', from)
    if (to.name !== 'index') {
      return { x: 0, y: 0 }
    }
  })
}
