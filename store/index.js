export const state = () => ({
  user: null,
  users: null,
  posts: null
})

export const mutations = {
  // General mutation
  setItem (state, { resource, payload }) {
    // console.log('·store· setItem:', resource, payload)
    state[resource] = payload
  }
}

export const actions = {
  loadUsers ({ state, commit }) {
    const resource = 'users'
    const userDefault = '1020739994'
    this.$db.ref(resource).on('value', (snapshot) => {
      if (snapshot.val()) {
        const payload = snapshot.val()
        commit('setItem', { resource, payload })
        this.dispatch('setUser', payload[userDefault] )
      }
    })
  },
  setUser({ state, commit }, payload) {
    commit('setItem', { resource: 'user', payload: payload })
  },
  loadPosts ({ state, commit }) {
    let items = []
    const resource = 'posts'
    this.$db.ref(resource).on('value', (snapshot) => {
      if (snapshot.val()) {
        // Agregar atributo ID para su gestión
        items = _.forEach(snapshot.val(), (item, key) => { item.id = key })
        // Filtrar items activos
        // items = _.filter(items, 'activo')
        // Ordenarlos
        const payload = _.orderBy(items, ['timestamp'], ['desc'])
        commit('setItem', { resource, payload })
      }
    })
  },
  newPost({ state, commit }, { path, object }) {
    // console.log('newPost', path, object)
    // Path si es nuevo post
    let initialPath = 'posts'
    // Ajustar path si es reply
    if (path) {
      initialPath = `posts/${path}/replies`
    }
    let newKey = this.$db.ref(initialPath).push().key
    // console.log('newKey', newKey)
    setTimeout(() => {
      // *** Crear publicación / comentario ***
      let updateObject = {}
      updateObject[`${initialPath}/${newKey}`] = object
      // console.log('updateObject', updateObject)
      this.$db.ref().update(updateObject)
    }, 200)
  },
  newReact({ state, commit }, { path, newValue }) {
    // console.log('newReact', path, newValue)
    // *** Actualizar reaccion ***
    let updateObject = {}
    updateObject[path] = newValue
    // console.log('updateObject', updateObject)
    this.$db.ref().update(updateObject)
  }
}

export const getters = {
  user: state => state.user,
  users: state => state.users,
  posts: state => state.posts
}
